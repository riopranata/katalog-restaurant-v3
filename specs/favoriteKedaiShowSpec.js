import FavoriteKedaiSearchView
  from '../src/scripts/views/pages/liked-kedais/favorite-kedai-search-view';
import FavoriteKedaiShowPresenter
  from '../src/scripts/views/pages/liked-kedais/favorite-kedai-show-presenter';
import FavoriteKedaiIdb from '../src/scripts/data/favorite-kedai-idb';
describe('Showing all favorite kedais', () => {
  let view;

  const renderTemplate = () => {
    view = new FavoriteKedaiSearchView();
    document.body.innerHTML = view.getTemplate();
  };

  beforeEach(() => {
    renderTemplate();
  });

  describe('When no kedais have been liked', () => {
    it('should ask for the favorite kedais', () => {
      const favoriteKedais = spyOnAllFunctions(FavoriteKedaiIdb);
      new FavoriteKedaiShowPresenter({
        view,
        favoriteKedais,
      });
      expect(favoriteKedais.getAllKedais).toHaveBeenCalledTimes(1);
    });
    it('should show the information that no kedais have been liked', (done) => {
      document.getElementById('kedaiFavorite').addEventListener('kedaiFavorite:updated', () => {
        expect(document.querySelectorAll('.empty-favorite-tag').length)
            .toEqual(1);
        done();
      });
      const favoriteKedais = spyOnAllFunctions(FavoriteKedaiIdb);
      favoriteKedais.getAllKedais.and.returnValues([]);
      new FavoriteKedaiShowPresenter({
        view,
        favoriteKedais,
      });
    });
  });

  describe('When favorite kedais exist', () => {
    it('should show the kedais', (done) => {
      document.getElementById('kedaiFavorite').addEventListener('kedaiFavorite:updated', () => {
        expect(document.querySelectorAll('.kedai-item').length).toEqual(2);
        done();
      });
      const favoriteKedais = spyOnAllFunctions(FavoriteKedaiIdb);
      favoriteKedais.getAllKedais.and.returnValues([
        {
          id: 11, name: 'A', rating: 3, description: 'Sebuah kedai A',
        },
        {
          id: 22, name: 'B', rating: 4, description: 'Sebuah kedai B',
        },
      ]);
      new FavoriteKedaiShowPresenter({
        view,
        favoriteKedais,
      });
    });
  });
});
