import FavoriteKedaiIdb from '../src/scripts/data/favorite-kedai-idb';
import * as TestFactories from './helpers/testFactories';

const addLikeButtonContainer = () => {
  document.body.innerHTML = '<div id="likeButtonContainer"></div>';
};
describe('Unliking A Kedai', () => {
  beforeEach(async () => {
    addLikeButtonContainer();
    await FavoriteKedaiIdb.putKedai({id: 1});
  });
  afterEach(async () => {
    await FavoriteKedaiIdb.deleteKedai(1);
  });

  it('should display unlike widget when the kedai has been liked', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({id: 1});

    expect(document.querySelector('[aria-label="unlike this kedai"]')).toBeTruthy();
  });

  it('should not display like widget when the kedai has been liked', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({id: 1});

    expect(document.querySelector('[aria-label="like this kedai"]')).toBeFalsy();
  });

  it('should be able to remove liked kedai from the list', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({id: 1});

    document.querySelector('[aria-label="unlike this kedai"]').dispatchEvent(new Event('click'));

    expect(await FavoriteKedaiIdb.getAllKedais()).toEqual([]);
  });

  it('should not throw error if the unliked kedai is not in the list', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({id: 1});

    // hapus dulu kedai dari daftar kedai yang disukai
    await FavoriteKedaiIdb.deleteKedai(1);

    // kemudian, simulasikan pengguna menekan widget batal menyukai kedai
    document.querySelector('[aria-label="unlike this kedai"]').dispatchEvent(new Event('click'));
    expect(await FavoriteKedaiIdb.getAllKedais()).toEqual([]);
  });
});
