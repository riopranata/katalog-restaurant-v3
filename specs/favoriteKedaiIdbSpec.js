import {itActsAsFavoriteKedaiModel} from './contract/favoriteKedaiContract';
import FavoriteKedaiIdb from '../src/scripts/data/favorite-kedai-idb';

describe('Favorite Kedai Idb Contract Test Implementation', () => {
  afterEach(async () => {
    (await FavoriteKedaiIdb.getAllKedais()).forEach(async (kedai) => {
      await FavoriteKedaiIdb.deleteKedai(kedai.id);
    });
  });

  itActsAsFavoriteKedaiModel(FavoriteKedaiIdb);
});
