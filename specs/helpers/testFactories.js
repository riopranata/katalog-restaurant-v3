import LikeButtonPresenter from '../../src/scripts/utils/like-button-presenter';
import FavoriteKedaiIdb from '../../src/scripts/data/favorite-kedai-idb';

const createLikeButtonPresenterWithKedai = async (kedai) => {
  await LikeButtonPresenter.init({
    likeButtonContainer: document.querySelector('#likeButtonContainer'),
    favoriteKedais: FavoriteKedaiIdb,
    kedai,
  });
};
export {createLikeButtonPresenterWithKedai};
