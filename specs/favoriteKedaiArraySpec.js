import {itActsAsFavoriteKedaiModel} from './contract/favoriteKedaiContract';
let favoriteKedais = [];
const FavoriteKedaiArray = {
  getKedai(id) {
    if (!id) {
      return;
    }
    return favoriteKedais.find((kedai) => kedai.id === id);
  },
  getAllKedais() {
    return favoriteKedais;
  },
  putKedai(kedai) {
    if (!kedai.hasOwnProperty('id')) {
      return;
    }
    // pastikan id ini belum ada dalam daftar favoriteKedais
    if (this.getKedai(kedai.id)) {
      return;
    }
    favoriteKedais.push(kedai);
  },
  deleteKedai(id) {
    // cara boros menghapus kedai dengan meng-copy kedai yang ada
    // kecuali kedai dengan id == id
    favoriteKedais = favoriteKedais.filter((kedai) => kedai.id !== id);
  },

  searchKedais(query) {
    return this.getAllKedais()
        .filter((kedai) => {
          const loweredCaseKedaiTitle = (kedai.name || '-').toLowerCase();
          const jammedKedaiTitle = loweredCaseKedaiTitle.replace(/\s/g, '');

          const loweredCaseQuery = query.toLowerCase();
          const jammedQuery = loweredCaseQuery.replace(/\s/g, '');

          return jammedKedaiTitle.indexOf(jammedQuery) !== -1;
        });
  },
};

describe('Favorite Kedai Array Contract Test Implementation', () => {
  afterEach(() => favoriteKedais = []);
  itActsAsFavoriteKedaiModel(FavoriteKedaiArray);
});
