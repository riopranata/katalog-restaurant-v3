import FavoriteKedaiSearchPresenter
  from '../src/scripts/views/pages/liked-kedais/favorite-kedai-search-presenter';
import FavoriteKedaiIdb from '../src/scripts/data/favorite-kedai-idb';
import FavoriteKedaiSearchView
  from '../src/scripts/views/pages/liked-kedais/favorite-kedai-search-view';

  describe('Searching kedais', () => {
  let presenter;
  let favoriteKedais;
  let view;
  const searchKedais = (query) => {
    const queryElement = document.getElementById('query');
    queryElement.value = query;
    queryElement.dispatchEvent(new Event('change'));
  };
  const setKedaiSearchContainer = () => {
    view = new FavoriteKedaiSearchView();
    document.body.innerHTML = view.getTemplate();
  };
  const constructPresenter = () => {
    favoriteKedais = spyOnAllFunctions(FavoriteKedaiIdb);
    presenter = new FavoriteKedaiSearchPresenter({
      favoriteKedais,
      view,
    });
  };
  beforeEach(() => {
    setKedaiSearchContainer();
    constructPresenter();
  });
  describe('When query is not empty', () => {
    it('should be able to capture the query typed by the user', () => {
      searchKedais('kedai a');
      expect(presenter.latestQuery)
          .toEqual('kedai a');
    });

    it('should ask the model to search for kedais', () => {
      searchKedais('kedai a');
      expect(favoriteKedais.searchKedais)
          .toHaveBeenCalledWith('kedai a');
    });

    it('should show the found kedais', () => {
      presenter._showFoundKedais([{id: 1}]);
      expect(document.querySelectorAll('.kedai-item').length)
          .toEqual(1);

      presenter._showFoundKedais([{
        id: 1,
        name: 'Satu',
      }, {
        id: 2,
        name: 'Dua',
      }]);
      expect(document.querySelectorAll('.kedai-item').length)
          .toEqual(2);
    });

    // it('should show the title of the found kedais', () => {
    //   presenter._showFoundKedais([{
    //     id: 1,
    //     name: 'Satu',
    //   }]);
    //   expect(document.querySelectorAll('.kedai-item')
    //       .item(1).textContent)
    //       .toEqual('satu');
    // });

    // it('should show - when the kedai returned does not contain a title', (done) => {
    //   document.getElementById('kedaiFavorite').addEventListener('kedaiFavorite:updated', () => {
    //     const kedaiTitles = document.querySelectorAll('.kedais-detail .kedai__title');
    //     expect(kedaiTitles.item(0).textContent).toEqual('-');

    //     done();
    //   });
    //   favoriteKedais.searchKedais.withArgs('kedai a').and.returnValues([
    //     {id: 444},
    //   ]);
    //   searchKedais('kedai a');
    // });
  });

  describe('When query is empty', () => {
    it('should capture the query as empty', () => {
      searchKedais(' ');
      expect(presenter.latestQuery.length)
          .toEqual(0);
      searchKedais('    ');
      expect(presenter.latestQuery.length)
          .toEqual(0);
      searchKedais('');
      expect(presenter.latestQuery.length)
          .toEqual(0);
      searchKedais('\t');
      expect(presenter.latestQuery.length)
          .toEqual(0);
    });

    it('should show all favorite kedais', () => {
      searchKedais('    ');
      expect(favoriteKedais.getAllKedais)
          .toHaveBeenCalled();
    });
  });

  describe('When no favorite kedais could be found', () => {
    it('should show the empty message', (done) => {
      document.getElementById('kedaiFavorite').addEventListener('kedaiFavorite:updated', () => {
        expect(document.querySelectorAll('.empty-favorite-tag').length).toEqual(1);

        done();
      });

      favoriteKedais.searchKedais.withArgs('kedai a').and.returnValues([]);

      searchKedais('kedai a');
    });

    it('should not show any kedai', (done) => {
      document.getElementById('kedaiFavorite').addEventListener('kedaiFavorite:updated', () => {
        expect(document.querySelectorAll('.kedai-item').length)
            .toEqual(0);
        done();
      });

      favoriteKedais.searchKedais.withArgs('kedai a')
          .and

          .returnValues([]);
      searchKedais('kedai a');
    });
  });
});
