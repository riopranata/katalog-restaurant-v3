const itActsAsFavoriteKedaiModel = (favoriteKedai) => {
  it('should return the Kedai that has been added', async () => {
    favoriteKedai.putKedai({id: 1});
    favoriteKedai.putKedai({id: 2});
    expect(await favoriteKedai.getKedai(1))
        .toEqual({id: 1});
    expect(await favoriteKedai.getKedai(2))
        .toEqual({id: 2});
    expect(await favoriteKedai.getKedai(3))
        .toEqual(undefined);
  });
  it('should refuse a Kedai from being added if it does not have the correct property', async () => {
    favoriteKedai.putKedai({aProperty: 'property'});
    expect(await favoriteKedai.getAllKedais())
        .toEqual([]);
  });
  it('can return all of the Kedais that have been added', async () => {
    favoriteKedai.putKedai({id: 1});
    favoriteKedai.putKedai({id: 2});
    expect(await favoriteKedai.getAllKedais())
        .toEqual([
          {id: 1},
          {id: 2},
        ]);
  });
  it('should remove favorite Kedai', async () => {
    favoriteKedai.putKedai({id: 1});
    favoriteKedai.putKedai({id: 2});
    favoriteKedai.putKedai({id: 3});
    await favoriteKedai.deleteKedai(1);
    expect(await favoriteKedai.getAllKedais())
        .toEqual([
          {id: 2},
          {id: 3},
        ]);
  });
  it('should handle request to remove a Kedai even though the Kedai has not been added', async () => {
    favoriteKedai.putKedai({id: 1});
    favoriteKedai.putKedai({id: 2});
    favoriteKedai.putKedai({id: 3});
    await favoriteKedai.deleteKedai(4);
    expect(await favoriteKedai.getAllKedais())
        .toEqual([
          {id: 1},
          {id: 2},
          {id: 3},
        ]);
  });

  it('should be able to search for Kedais', async () => {
    favoriteKedai.putKedai({id: 1, name: 'Kedai a'});
    favoriteKedai.putKedai({id: 2, name: 'Kedai b'});
    favoriteKedai.putKedai({id: 3, name: 'Kedai abc'});
    favoriteKedai.putKedai({id: 4, name: 'ini mah Kedai abcd'});

    expect(await favoriteKedai.searchKedais('Kedai a')).toEqual([
      {id: 1, name: 'Kedai a'},
      {id: 3, name: 'Kedai abc'},
      {id: 4, name: 'ini mah Kedai abcd'},
    ]);
  });
};

export {itActsAsFavoriteKedaiModel};
