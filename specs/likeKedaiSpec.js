import FavoriteKedaiIdb from '../src/scripts/data/favorite-kedai-idb';
import * as TestFactories from './helpers/testFactories';

describe('Liking A Kedai', () => {
  const addLikeButtonContainer = () => {
    document.body.innerHTML = '<div id="likeButtonContainer"></div>';
  };
  beforeEach(() => {
    addLikeButtonContainer();
  });

  it('should show the like button when the kedai has not been liked before', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({id: 1});

    expect(document.querySelector('[aria-label="like this kedai"]'))
        .toBeTruthy();
  });

  it('should not show the unlike button when the kedai has not been liked before', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({id: 1});

    expect(document.querySelector('[aria-label="unlike this kedai"]')).toBeFalsy();
  });

  it('should be able to like the kedai', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({id: 1});

    document.querySelector('#likeButton').dispatchEvent(new Event('click'));
    const kedai = await FavoriteKedaiIdb.getKedai(1);

    expect(kedai).toEqual({id: 1});
    FavoriteKedaiIdb.deleteKedai(1);
  });

  it('should not add a kedai again when its already liked', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({id: 1});

    // Tambahkan kedai dengan ID 1 ke daftar kedai yang disukai
    await FavoriteKedaiIdb.putKedai({id: 1});

    // Simulasikan pengguna menekan tombol suka kedai
    document.querySelector('#likeButton').dispatchEvent(new Event('click'));
    // tidak ada kedai yang ganda
    expect(await FavoriteKedaiIdb.getAllKedais()).toEqual([{id: 1}]);
    FavoriteKedaiIdb.deleteKedai(1);
  });

  it('should not add a kedai when it has no id', async () => {
    await TestFactories.createLikeButtonPresenterWithKedai({});

    document.querySelector('#likeButton').dispatchEvent(new Event('click'));

    expect(await FavoriteKedaiIdb.getAllKedais()).toEqual([]);
  });
});
