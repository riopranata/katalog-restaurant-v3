import CONFIG from '../../globals/config';

const createJumbotronHero = `
  <div class="hero">
      <picture>
        <source media="(max-width: 600px)" srcset="./heros/hero-image_2-small.webp" type="image/webp">  
        <source media="(max-width: 600px)" srcset="./heros/hero-image_2-small.jpg" type="image/jpg">
          <img
            class="lazyload"
            src="./heros/placeholder.jpg" width="500" height="250" 
            srcset='./heros/hero-image_2-large.jpg'
            sizes="(max-width: 600px) 480px, 800px"
            alt="heroImage">
          </img>  
      </picture>
      <div class="title">
        <h4>KEDAI KONGKOW</h4>
        <p>Rekomendasi Terbaik dari Kami</p>
      </div>
  </div>
`;

const createKedaiItemDetailTemplate = (kedai) => `
<h2 class="kedai__title">${kedai.restaurant.name}</h2>

<picture>
  <source media="(max-width: 600px)" data-srcset="${CONFIG.BASE_IMAGE_URL}medium/${kedai.restaurant.pictureId}">
    <img 
      class="kedai__poster lazyload"
      src="./heros/placeholder.jpg" width="500" height="250"
      data-src='${CONFIG.BASE_IMAGE_URL}small/${kedai.restaurant.pictureId}' 
      alt="${kedai.restaurant.name}">
    </img>  
</picture>

<div class="kedai__info">
  <h3>Information</h3>
  <table>
    <tr>
      <th>Alamat</th>
      <th>:</th>
      <td>${kedai.restaurant.address}, ${kedai.restaurant.city}</td>
    </tr>
    <tr>
      <th>Informasi</th>
      <th>:</th>
      <td>${kedai.restaurant.description}</td>
    </tr>
  </table>
  <div class="category">
    <div class="category-menu">
      <table>
        <tr>
          <th>Rating</th>
          <th>:</th>
          <td>${kedai.restaurant.rating}  <span style="color:yellow" class="stars kedai-item__header__rating__score" data-rating="${kedai.restaurant.rating}" data-num-stars="5"></span></td>
        </tr>
        <tr>
        <tr>
          <th>Kategori Menu</th>
          <th>:</th>
          <td>${kedai.restaurant.categories.map((categori) => `<ul><li class="category-name">${categori.name}</li></ul>`).join('')}</td>
        </tr>
        <tr>
          <th>Kedaian</th>
          <th>:</th>
          <td>${kedai.restaurant.menus.foods.map((food) => `<ul><li class="category-name">${food.name}</li></ul>`).join('')}</td>
        </tr>
        <tr>
          <th>Minuman</th>
          <th>:</th>
          <td>${kedai.restaurant.menus.drinks.map((drink) => `<ul><li class="category-name">${drink.name}</li></ul>`).join('')}</td>
        </tr>
      </table>
      <h3>Customer Review</h3>
      <div class="review">${kedai.restaurant.customerReviews.map((review) => `
        <div class="review-card">
          <i class="fas fa-user-circle"></i>
          <p class="review-name">${review.name}</p>
          <p class="review-comment">${review.review}</p>
          <p class="review-date">${review.date}</p>
        </div>
        `).join('')}
      </div>
    </div>
`;

const createKedaiItemTemplate = (kedai) => `
<div class="kedai-item">
    <div class="kedai-item__header">
        
        <picture>
          <source media="(max-width: 600px)" data-srcset="${CONFIG.BASE_IMAGE_URL}medium/${kedai.pictureId}">
            <img 
              class="kedai-item__header__poster lazyload"
              src="./heros/placeholder.jpg" width="500" height="250"
              data-src='${CONFIG.BASE_IMAGE_URL}small/${kedai.pictureId}' 
              alt="${kedai.name || '-'}">
            </img>  
        </picture>

        <div class="kedai-item__header__rating">
            <p>${kedai.city}</p>
            <span class="stars kedai-item__header__rating__score" data-rating="${kedai.rating || '-'}" data-num-stars="5"></span>
        </div>
    </div>
    <div class="kedai-item__content">
        <h3 class="kedai__title">
            <a href="${`/#/detail/${kedai.id}`}">${kedai.name || '-'}</a>
        </h3>
        <p>${kedai.description || '-'}</p>
        <span><button aria-label="Booking ${kedai.name}">Booking</button></span>
    </div>
</div>
`;

const createLikeKedaiButtonTemplate = () => `
  <button aria-label="like this kedai" id="likeButton" class="like">
     <i class="fa fa-heart-o" aria-hidden="true"></i>
  </button>
`;

const createUnlikeKedaiButtonTemplate = () => `
  <button aria-label="unlike this kedai" id="likeButton" class="like">
    <i class="fa fa-heart" aria-hidden="true"></i>
  </button>
`;

const aboutUsInfo = `
  <div class="aboutus">

      <picture>
        <source media="(max-width: 600px)" srcset="./heros/under_development-small.webp" type="image/webp">  
        <source media="(max-width: 600px)" srcset="./heros/under_development-small.jpg" type="image/jpeg">
          <img
            class="lazyload"
            src="./heros/placeholder.jpg" width="500" height="250"
            srcset='./heros/under_development-large.jpg' 
            alt="Dalam Pengembangan">
          </img>  
      </picture>

      <div>
        <h4>Masih dalam Pengembangan</h4>
        <p>Menunggu Hasil Review dari para Reviewer Dicoding</p>
      </div>
  </div>
`;

const favoriteInfo = `
<div class="empty-favorite-tag">
  <i class="fas fa-heart-broken">
  <p>
  Maaf, belum ada Kedai Favorit yang bisa disajikan, ayo jadi yang pertama !!! 
  </p>
</div>
`;

export {
  createKedaiItemTemplate,
  createKedaiItemDetailTemplate,
  createJumbotronHero,
  createLikeKedaiButtonTemplate,
  createUnlikeKedaiButtonTemplate,
  aboutUsInfo,
  favoriteInfo,
};
