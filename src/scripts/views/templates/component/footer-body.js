class footerBody extends HTMLElement {
  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = `
      <footer>
      <span>Copyright @2021 - KEDAI KONGKOW</span>
    </footer>
          `;
  }
}

customElements.define('footer-body', footerBody);
