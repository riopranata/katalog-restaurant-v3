class FavoriteKedaiShowPresenter {
  constructor({view, favoriteKedais}) {
    this._view = view;
    this._favoriteKedais = favoriteKedais;

    this._showFavoriteKedais();
  }

  async _showFavoriteKedais() {
    const kedaiFavorite = await this._favoriteKedais.getAllKedais();
    this._displayKedais(kedaiFavorite);
  }

  _displayKedais(kedaiFavorite) {
    this._view.showFavoriteKedais(kedaiFavorite);
  }
}

export default FavoriteKedaiShowPresenter;
