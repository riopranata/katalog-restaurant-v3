import {createKedaiItemTemplate} from '../../templates/template-creator';

class FavoriteKedaiSearchView {
  getTemplate() {
    return `
         <div class="content">
         <label>cari<input id="query" type="text"></label
         <div>
         <h2 class="content__heading">Paling Favorite</h2>
         </div>
             <div id="kedaiFavorite" class="kedais">

           </div>
       </div>
   `;
  }

  runWhenUserIsSearching(callback) {
    document.getElementById('query').addEventListener('change', (event) => {
      callback(event.target.value);
    });
  }

  showKedais(kedaiFavorite) {
    this.showFavoriteKedais(kedaiFavorite);
  }

  showFavoriteKedais(kedaiFavorite = []) {
    let html;
    if (kedaiFavorite.length) {
      html = kedaiFavorite.reduce((carry, kedaiFavorite) => carry.concat(createKedaiItemTemplate(kedaiFavorite)), '');
    } else {
      html = this._getEmptyKedaiTemplate();
    }

    document.getElementById('kedaiFavorite').innerHTML = html;

    document.getElementById('kedaiFavorite').dispatchEvent(new Event('kedaiFavorite:updated'));
  }

  _getEmptyKedaiTemplate() {
    return `
    <div class="empty-favorite-tag">
        <i class="fas fa-heart-broken"></i>
        <p>
        Maaf, belum ada Kedai Favorit yang bisa disajikan, ayo jadi yang pertama !!! 
        </p>
    </div>
    `;
  }
}

export default FavoriteKedaiSearchView;
