class FavoriteKedaiSearchPresenter {
  constructor({favoriteKedais, view}) {
    this._view = view;
    this._listenToSearchRequestByUser();
    this._favoriteKedais = favoriteKedais;
  }
  _listenToSearchRequestByUser() {
    this._view.runWhenUserIsSearching((latestQuery) => {
      this._searchKedais(latestQuery);
    });
  }
  async _searchKedais(latestQuery) {
    this._latestQuery = latestQuery.trim();
    let foundKedais;
    if (this.latestQuery.length > 0) {
      foundKedais = await this._favoriteKedais.searchKedais(this.latestQuery);
    } else {
      foundKedais = await this._favoriteKedais.getAllKedais();
    }
    this._showFoundKedais(foundKedais);
  }

  _showFoundKedais(kedaiFavorite) {
    this._view.showFavoriteKedais(kedaiFavorite);
  }

  get latestQuery() {
    return this._latestQuery;
  }
}
export default FavoriteKedaiSearchPresenter;
