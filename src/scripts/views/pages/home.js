import TheKedaiDbSource from '../../data/thekedaidb-source';
import {createKedaiItemTemplate, createJumbotronHero} from '../templates/template-creator';

const Home = {
  async render() {
    return `
      <div class="jumbotron" id="jumbotron">
       
      </div>

      <div class="content">
          <h2 class="content__heading">Check This Kedai</h2>
          <div id="kedaiList" class="kedais">
          </div>
      </div>
      `;
  },

  async afterRender() {
    // Menetapkan pemanggilan sumber data dari config.js
    const kedaiList = await TheKedaiDbSource.home();

    // Menetapkan kontainer dan css untuk menampilkan jumbotron/hero/header
    const jumbotronContainer = document.querySelector('#jumbotron');
    // Menampilkan isi data dari createJumbotronHero yang di set dari file template-creater
    jumbotronContainer.innerHTML += createJumbotronHero;

    // Menetapkan Kontainer dan css data yang akan digunakan
    const kedaiListContainer = document.querySelector('#kedaiList');

    $(function() {
      $('.stars').stars();
    });
    $.fn.stars = function() {
      return $(this).each(function() {
        const rating = $(this).data('rating');
        const numStars = $(this).data('numStars');
        const fullStar = '<i class="fas fa-star"></i>'.repeat(Math.floor(rating));
        const halfStar = (rating%1!== 0) ? '<i class="fas fa-star-half-alt"></i>': '';
        const noStar = '<i class="far fa-star"></i>'.repeat(Math.floor(numStars-rating));
        $(this).html(`${fullStar}${halfStar}${noStar}`);
      });
    };

    // Menampilkan isi data dari createKedaiItemTemplate yang di set dari file template-creater
    kedaiList.forEach((kedaiItem) => {
      kedaiListContainer.innerHTML += createKedaiItemTemplate(kedaiItem);
    });
  },
};

export default Home;
