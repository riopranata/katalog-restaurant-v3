import UrlParser from '../../routes/url-parser';
import TheKedaiDbSource from '../../data/thekedaidb-source';
import {createKedaiItemDetailTemplate} from '../templates/template-creator';
import LikeButtonPresenter from '../../utils/like-button-presenter';
import FavoriteKedaiIdb from '../../data/favorite-kedai-idb';

const Detail = {
  async render() {
    return `
        <article id="kedai_list_detail" class="kedais-detail"></article>
        <div id="likeButtonContainer"></div>
      `;
  },

  async afterRender() {
    // menetapkan url parser yang akan digunakan (sumber file dari api-endpoint.js)
    const url = UrlParser.parseActiveUrlWithoutCombiner();
    // menetapkan fungsi dan sumber data fungsi yang akan digunakan (sumber file dari api-endpoint.js)
    const kedai = await TheKedaiDbSource.detail(url.id);

    // Menetapkan Kontainer dan css data yang akan digunakan
    const kedaiDetailContainer = document.querySelector('#kedai_list_detail');
    kedaiDetailContainer.innerHTML = createKedaiItemDetailTemplate(kedai);

    $(function() {
      $('.stars').stars();
    });
    $.fn.stars = function() {
      return $(this).each(function() {
        const rating = $(this).data('rating');
        const numStars = $(this).data('numStars');
        const fullStar = '<i class="fas fa-star"></i>'.repeat(Math.floor(rating));
        const halfStar = (rating%1!== 0) ? '<i class="fas fa-star-half-alt"></i>': '';
        const noStar = '<i class="far fa-star"></i>'.repeat(Math.floor(numStars-rating));
        $(this).html(`${fullStar}${halfStar}${noStar}`);
      });
    };

    LikeButtonPresenter.init({
      likeButtonContainer: document.querySelector('#likeButtonContainer'),
      favoriteKedais: FavoriteKedaiIdb,
      kedai: {
        id: kedai.restaurant.id,
        city: kedai.restaurant.city,
        name: kedai.restaurant.name,
        description: kedai.restaurant.description,
        pictureId: kedai.restaurant.pictureId,
        rating: kedai.restaurant.rating,
      },
    });
  },
};

export default Detail;
