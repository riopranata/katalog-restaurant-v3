import FavoriteKedaiIdb from '../../data/favorite-kedai-idb';
import {createKedaiItemTemplate, favoriteInfo} from '../templates/template-creator';
import FavoriteKedaiSearchView from './liked-kedais/favorite-kedai-search-view';
import FavoriteKedaiShowPresenter from './liked-kedais/favorite-kedai-show-presenter';
import FavoriteKedaiSearchPresenter from './liked-kedais/favorite-kedai-search-presenter';

const view = new FavoriteKedaiSearchView();

const Favorite = {
  async render() {
    return view.getTemplate();
  },

  async afterRender() {
    new FavoriteKedaiShowPresenter({view, favoriteKedais: FavoriteKedaiIdb});
    new FavoriteKedaiSearchPresenter({view, favoriteKedais: FavoriteKedaiIdb});
    const kedais = await FavoriteKedaiIdb.getAllKedais();
    const kedaisContainer = document.querySelector('#query');

    $(function() {
      $('.stars').stars();
    });
    $.fn.stars = function() {
      return $(this).each(function() {
        const rating = $(this).data('rating');
        const numStars = $(this).data('numStars');
        const fullStar = '<i class="fas fa-star"></i>'.repeat(Math.floor(rating));
        const halfStar = (rating%1!== 0) ? '<i class="fas fa-star-half-alt"></i>': '';
        const noStar = '<i class="far fa-star"></i>'.repeat(Math.floor(numStars-rating));
        $(this).html(`${fullStar}${halfStar}${noStar}`);
      });
    };

    if (kedais.length > 0) {
      kedais.map((kedai) => {
        console.log(kedai.name);
        kedaisContainer.innerHTML += createKedaiItemTemplate(kedai);
      });
    } else {
      kedaisContainer.innerHTML += favoriteInfo;
    }
  },
};

export default Favorite;
