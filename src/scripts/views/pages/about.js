import {aboutUsInfo} from '../templates/template-creator';

const About = {
  async render() {
    return `
    <div class="jumbotron" id="aboutus">
       
      </div>
        `;
  },

  async afterRender() {
    // Menetapkan kontainer dan css untuk menampilkan jumbotron/hero/header
    const aboutUsContainer = document.querySelector('#aboutus');
    // Menampilkan isi data dari createJumbotronHero yang di set dari file template-creater
    aboutUsContainer.innerHTML += aboutUsInfo;
  },
};

export default About;
