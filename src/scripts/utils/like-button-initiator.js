import FavoriteKedaiIdb from '../data/favorite-kedai-idb';
import {
  createLikeKedaiButtonTemplate,
  createUnlikeKedaiButtonTemplate,
} from '../views/templates/template-creator';

const LikeButtonInitiator = {
  async init({likeButtonContainer, kedai}) {
    this._likeButtonContainer = likeButtonContainer;
    this._kedai = kedai;

    await this._renderButton();
  },

  async _renderButton() {
    const {id} = this._kedai;

    if (await this._isKedaiExist(id)) {
      this._renderLiked();
    } else {
      this._renderLike();
    }
  },

  async _isKedaiExist(id) {
    const kedai = await FavoriteKedaiIdb.getKedai(id);
    return !!kedai;
  },

  _renderLike() {
    this._likeButtonContainer.innerHTML = createLikeKedaiButtonTemplate();

    const likeButton = document.querySelector('#likeButton');
    likeButton.addEventListener('click', async () => {
      await FavoriteKedaiIdb.putKedai(this._kedai);
      this._renderButton();
    });
  },

  _renderLiked() {
    this._likeButtonContainer.innerHTML = createUnlikeKedaiButtonTemplate();

    const likeButton = document.querySelector('#likeButton');
    likeButton.addEventListener('click', async () => {
      await FavoriteKedaiIdb.deleteKedai(this._kedai.id);
      this._renderButton();
    });
  },
};

export default LikeButtonInitiator;
