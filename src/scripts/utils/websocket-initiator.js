import NotificationHelper from './notification-helper';

const WebSocketInitiator = {
  init(url) {
    const webSocket = new WebSocket(url);
    webSocket.onmessage = this._onMessageHandler;
  },

  _onMessageHandler(message) {
    console.log(message.data);
    NotificationHelper.sendNotification({
      title: `Notification From Kedai`,
      options: {
        body: message.data,
        icon: 'icons/favicon.png',
        image: 'heros/hero-image_2.jpg',
      },
    });
  },
};

export default WebSocketInitiator;
