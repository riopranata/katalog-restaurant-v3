import {
  createLikeKedaiButtonTemplate,
  createUnlikeKedaiButtonTemplate,
} from '../views/templates/template-creator';

const LikeButtonPresenter = {
  async init({likeButtonContainer, favoriteKedais, kedai}) {
    this._likeButtonContainer = likeButtonContainer;
    this._kedai = kedai;
    this._favoriteKedais = favoriteKedais;

    await this._renderButton();
  },

  async _renderButton() {
    const {id} = this._kedai;
    if (await this._isKedaiExist(id)) {
      this._renderLiked();
    } else {
      this._renderLike();
    }
  },

  async _isKedaiExist(id) {
    const kedai = await this._favoriteKedais.getKedai(id);
    return !!kedai;
  },

  _renderLike() {
    this._likeButtonContainer.innerHTML = createLikeKedaiButtonTemplate();

    const likeButton = document.querySelector('#likeButton');
    likeButton.addEventListener('click', async () => {
      await this._favoriteKedais.putKedai(this._kedai);
      this._renderButton();
    });
  },

  _renderLiked() {
    this._likeButtonContainer.innerHTML = createUnlikeKedaiButtonTemplate();

    const likeButton = document.querySelector('#likeButton');
    likeButton.addEventListener('click', async () => {
      await this._favoriteKedais.deleteKedai(this._kedai.id);
      this._renderButton();
    });
  },

};
export default LikeButtonPresenter;
