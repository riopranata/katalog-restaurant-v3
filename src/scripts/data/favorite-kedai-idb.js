import {openDB} from 'idb';
import CONFIG from '../globals/config';
const {DATABASE_NAME, DATABASE_VERSION, OBJECT_STORE_NAME} = CONFIG;
const dbPromise = openDB(DATABASE_NAME, DATABASE_VERSION, {
  upgrade(database) {
    database.createObjectStore(OBJECT_STORE_NAME, {keyPath: 'id'});
  },
});

const FavoriteKedaiIdb = {
  async getKedai(id) {
    if (!id) {
      return;
    }

    return (await dbPromise).get(OBJECT_STORE_NAME, id);
  },

  async getAllKedais() {
    return (await dbPromise).getAll(OBJECT_STORE_NAME);
  },
  async putKedai(kedai) {
    if (!kedai.hasOwnProperty('id')) {
      return;
    }

    return (await dbPromise).put(OBJECT_STORE_NAME, kedai);
  },
  async deleteKedai(id) {
    return (await dbPromise).delete(OBJECT_STORE_NAME, id);
  },
  async searchKedais(query) {
    return (await this.getAllKedais()).filter((kedai) => {
      const loweredCaseKedaiTitle = (kedai.name || '-').toLowerCase();
      const jammedKedaiTitle = loweredCaseKedaiTitle.replace(/\s/g, '');

      const loweredCaseQuery = query.toLowerCase();
      const jammedQuery = loweredCaseQuery.replace(/\s/g, '');

      return jammedKedaiTitle.indexOf(jammedQuery) !== -1;
    });
  },
};
export default FavoriteKedaiIdb;
