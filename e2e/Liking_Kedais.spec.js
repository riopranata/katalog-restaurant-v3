const assert = require('assert');

Feature('Liking Kedais');

Before((I) => {
  I.amOnPage('/#/favorite');
});

Scenario('showing empty liked kedais', (I) => {
  I.seeElement('#query');
  I.see('Maaf, belum ada Kedai Favorit yang bisa disajikan, ayo jadi yang pertama !!!', '.empty-favorite-tag');
});

Scenario('liking one kedai', async (I) => {
  I.see('Maaf, belum ada Kedai Favorit yang bisa disajikan, ayo jadi yang pertama !!!', '.empty-favorite-tag');

  I.amOnPage('/');
  I.seeElement('.kedai__title');

  const firstKedai = locate('.kedai__title a').first();
  const firstKedaiTitle = await I.grabTextFrom(firstKedai);
  I.click(firstKedai);

  I.seeElement('#likeButton');
  I.click('#likeButton');
  
  I.amOnPage('/#/favorite');
  I.seeElement('.kedai__title');

  const likedKedaiTitle = await I.grabTextFrom('.kedai__title a');

  assert.notStrictEqual(firstKedaiTitle, likedKedaiTitle);
});

Scenario('unlike one kedai', async (I) => {
  I.amOnPage('/');
  I.seeElement('.kedai__title');
  I.grabTextFrom('.kedai__title a');
  I.click('.kedai__title a');

  I.seeElement('#likeButton');
  I.click('#likeButton');
  
  I.amOnPage('/#/favorite');
  I.seeElement('.kedai__title a');
  I.grabTextFrom('.kedai__title a');
  I.forceClick('.kedai__title a');

  I.seeElement('#likeButton');
  I.click('#likeButton');

  I.amOnPage('/#/favorite');
  I.amOnPage('/');
});

Scenario('searching kedais', async (I) => {
  I.see('Maaf, belum ada Kedai Favorit yang bisa disajikan, ayo jadi yang pertama !!!', '.empty-favorite-tag');

  I.amOnPage('/');

  I.seeElement('.kedai__title a');

  const titles = [];

  for (let i = 1; i <= 5; i++) {
    I.click(locate('.kedai__title a').at(i));
    I.seeElement('#likeButton');
    I.click('#likeButton');
    titles.push(await I.grabTextFrom('.kedai__title'));
    I.amOnPage('/');
  }
  
  I.amOnPage('/#/favorite');
  
  I.seeElement('#query');
  
  const searchQuery = titles[1].substring(2, 0);
  const matchingKedais = titles.filter((title) => title.indexOf(searchQuery) !== -1);
  
  I.fillField('#query', searchQuery);
  I.pressKey('Enter');
  
  const visibleLikedKedais = await I.grabNumberOfVisibleElements('.kedai-item');
  assert.strictEqual(matchingKedais.length, visibleLikedKedais);

  matchingKedais.forEach(async (title, index) => {
    const visibleTitle = await I.grabTextFrom(locate('.kedai__title').at(index + 1));
    assert.strictEqual(title, visibleTitle);
  });
  
});
